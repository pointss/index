var demoWorkspace = Blockly.inject('blocklyDiv', {
    toolbox: document.getElementById('toolbox')
});
if(document.cookie==""){
    document.cookie=createHexRandom()
}
Blockly.Xml.domToWorkspace(document.getElementById('sd'), demoWorkspace);
function copyToClip(content, message) {
    var aux = document.createElement('input');
    aux.setAttribute('value', content);
    document.body.appendChild(aux);
    aux.select();
    document.execCommand('copy');
    document.body.removeChild(aux);
    if (message == null) {
        mdui.snackbar({message:"复制成功！",position: 'top',});
    } else{
        mdui.snackbar({message,position: 'top',});
    }
}
function isVip(){
    var vips=["568712ba16e5e2d2a7b5cbc4f18ca6d314b6e315aff2f4e2fb611caf"]
    if(vips){
        if (vips.includes(document.cookie)){
            return true;
        }else if(document.cookie==""){
            document.cookie=createHexRandom()
            return false;
        }
        else{return false;}
    }
}
token=()=>{
    mdui.alert(document.cookie)
}
function showjs() {
    Blockly.JavaScript.INFINITE_LOOP_TRAP = null;
    var code = Blockly.JavaScript.workspaceToCode(demoWorkspace);
    var filncode;
    if(/*isVip()*/false){filncode="(async function(){console.clear();"+code+"}"}else{filncode = "(async function(){console.clear();"+encode(code)+"})()";}
    copyToClip(filncode);
}
function wbsjjm() {
    window.open("");
}
async function ptjs(e){
    if(e=='d'){
        function readfile() {
            wks=demoWorkspace;
            var e = prompt("请输入文件内容");
            var xml = Blockly.Xml.textToDom(e);
            Blockly.Xml.domToWorkspace(xml, demoWorkspace);
        }
        readfile()
    }else{
        var xml = Blockly.Xml.workspaceToDom(demoWorkspace);
        var xml_text = Blockly.Xml.domToText(xml);
        exportRaw('myblocks.xml',xml_text);
        function fakeClick(obj) {
            var ev = document.createEvent("MouseEvents");
            ev.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
            obj.dispatchEvent(ev);
        }

        function exportRaw(name, data) {
            var urlObject = window.URL || window.webkitURL || window;
            var export_blob = new Blob([data]);
            var save_link = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
            save_link.href = urlObject.createObjectURL(export_blob);
            save_link.download = name;
            fakeClick(save_link);
        }
    }
}
function createHexRandom(){ 
    var num = '';
 for (i = 0; i <= 55; i++)
 {
    var tmp = Math.ceil(Math.random()*15);
    if(tmp > 9){
           switch(tmp){ 
               case(10):
                   num+='a';
                   break;
               case(11):
                   num+='b';
                   break;
               case(12):
                   num+='c';
                   break;
               case(13):
                   num+='d';
                   break;
               case(14):
                   num+='e';
                   break;
               case(15):
                   num+='f';
                   break;
           }
        }else{
           num+=tmp;
        }
 }
 return num
}

String.prototype.has = function(c) {
    return this.indexOf(c) > -1;
}
;
function jsmin(comment, input, level) {
    if (input === undefined) {
        input = comment;
        comment = '';
        level = 2;
    } else if (level === undefined || level < 1 || level > 3) {
        level = 2;
    }
    if (comment.length > 0) {
        comment += '\n';
    }
    var a = ''
      , b = ''
      , EOF = -1
      , LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
      , DIGITS = '0123456789'
      , ALNUM = LETTERS + DIGITS + '_$\\'
      , theLookahead = EOF;
    function isAlphanum(c) {
        return c != EOF && (ALNUM.has(c) || c.charCodeAt(0) > 126);
    }
    function get() {
        var c = theLookahead;
        if (get.i == get.l) {
            return EOF;
        }
        theLookahead = EOF;
        if (c == EOF) {
            c = input.charAt(get.i);
            ++get.i;
        }
        if (c >= ' ' || c == '\n') {
            return c;
        }
        if (c == '\r') {
            return '\n';
        }
        return ' ';
    }
    get.i = 0;
    get.l = input.length;
    function peek() {
        theLookahead = get();
        return theLookahead;
    }
    function next() {
        var c = get();
        if (c == '/') {
            switch (peek()) {
            case '/':
                for (; ; ) {
                    c = get();
                    if (c <= '\n') {
                        return c;
                    }
                }
                break;
            case '*':
                get();
                for (; ; ) {
                    switch (get()) {
                    case '*':
                        if (peek() == '/') {
                            get();
                            return ' ';
                        }
                        break;
                    case EOF:
                        throw 'Error: Unterminated comment.';
                    }
                }
                break;
            default:
                return c;
            }
        }
        return c;
    }
    function action(d) {
        var r = [];
        if (d == 1) {
            r.push(a);
        }
        if (d < 3) {
            a = b;
            if (a == '\'' || a == '"') {
                for (; ; ) {
                    r.push(a);
                    a = get();
                    if (a == b) {
                        break;
                    }
                    if (a <= '\n') {
                        throw 'Error: unterminated string literal: ' + a;
                    }
                    if (a == '\\') {
                        r.push(a);
                        a = get();
                    }
                }
            }
        }
        b = next();
        if (b == '/' && '(,=:[!&|'.has(a)) {
            r.push(a);
            r.push(b);
            for (; ; ) {
                a = get();
                if (a == '/') {
                    break;
                } else if (a == '\\') {
                    r.push(a);
                    a = get();
                } else if (a <= '\n') {
                    throw 'Error: unterminated Regular Expression literal';
                }
                r.push(a);
            }
            b = next();
        }
        return r.join('');
    }
    function m() {
        var r = [];
        a = '\n';
        r.push(action(3));
        while (a != EOF) {
            switch (a) {
            case ' ':
                if (isAlphanum(b)) {
                    r.push(action(1));
                } else {
                    r.push(action(2));
                }
                break;
            case '\n':
                switch (b) {
                case '{':
                case '[':
                case '(':
                case '+':
                case '-':
                    r.push(action(1));
                    break;
                case ' ':
                    r.push(action(3));
                    break;
                default:
                    if (isAlphanum(b)) {
                        r.push(action(1));
                    } else {
                        if (level == 1 && b != '\n') {
                            r.push(action(1));
                        } else {
                            r.push(action(2));
                        }
                    }
                }
                break;
            default:
                switch (b) {
                case ' ':
                    if (isAlphanum(a)) {
                        r.push(action(1));
                        break;
                    }
                    r.push(action(3));
                    break;
                case '\n':
                    if (level == 1 && a != '\n') {
                        r.push(action(1));
                    } else {
                        switch (a) {
                        case '}':
                        case ']':
                        case ')':
                        case '+':
                        case '-':
                        case '"':
                        case '\'':
                            if (level == 3) {
                                r.push(action(3));
                            } else {
                                r.push(action(1));
                            }
                            break;
                        default:
                            if (isAlphanum(a)) {
                                r.push(action(1));
                            } else {
                                r.push(action(3));
                            }
                        }
                    }
                    break;
                default:
                    r.push(action(1));
                    break;
                }
            }
        }
        return r.join('');
    }
    jsmin.oldSize = input.length;
    ret = m(input);
    jsmin.newSize = ret.length;
    return comment + ret;
}
function compile(code)  {    
   var c=String.fromCharCode(code.charCodeAt(0)+code.length);  
   for(var i=1;i<code.length;i++){  
   c+=String.fromCharCode(code.charCodeAt(i)+code.charCodeAt(i-1));  
   }  
   return(escape(c));  
}  
function encode(code) {
    code = code.replace(/[\r\n]+/g, ''); 
    code = code.replace(/'/g, "\\'"); 
    var tmp = code.match(/\b(\w+)\b/g); 
    tmp.sort(); 
    var dict = []; 
    var i, t = ''; 
    for(var i=0; i<tmp.length; i++) { 
      if(tmp[i] != t) dict.push(t = tmp[i]); 
    } 
    var len = dict.length; 
    var ch; 
    for(i=0; i<len; i++) { 
      ch = num(i); 
      code = code.replace(new RegExp('\\b'+dict[i]+'\\b','g'), ch); 
      if(ch == dict[i]) dict[i] = ''; 
    } 
    var c=`var _0x83a2=\`我不是吉吉喵\`;if(_0x83a2!=decodeURI("%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5")){eval(decodeURI("%E6%8A%8A%E2%80%9C%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5%E2%80%9D%E5%8A%A0%E5%9B%9E%E6%9D%A5%EF%BC%81"))};eval(function(p,a,c,k,e,d){;if(_0x83a2!=decodeURI("%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5")){eval(decodeURI("%E6%8A%8A%E2%80%9C%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5%E2%80%9D%E5%8A%A0%E5%9B%9E%E6%9D%A5%EF%BC%81"))};e=function(c){;if(_0x83a2!=decodeURI("%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5")){eval(decodeURI("%E6%8A%8A%E2%80%9C%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5%E2%80%9D%E5%8A%A0%E5%9B%9E%E6%9D%A5%EF%BC%81"))};return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){;if(_0x83a2!=decodeURI("%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5")){eval(decodeURI("%E6%8A%8A%E2%80%9C%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5%E2%80%9D%E5%8A%A0%E5%9B%9E%E6%9D%A5%EF%BC%81"))};while(c--)d[e(c)]=k[c]||e(c);if(_0x83a2!=decodeURI("%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5")){eval(decodeURI("%E6%8A%8A%E2%80%9C%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5%E2%80%9D%E5%8A%A0%E5%9B%9E%E6%9D%A5%EF%BC%81"))};k=[function(e){;if(_0x83a2!=decodeURI("%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5")){eval(decodeURI("%E6%8A%8A%E2%80%9C%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5%E2%80%9D%E5%8A%A0%E5%9B%9E%E6%9D%A5%EF%BC%81"))};return d[e]}];if(_0x83a2!=decodeURI("%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5")){eval(decodeURI("%E6%8A%8A%E2%80%9C%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5%E2%80%9D%E5%8A%A0%E5%9B%9E%E6%9D%A5%EF%BC%81"))};e=function(){;if(_0x83a2!=decodeURI("%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5")){eval(decodeURI("%E6%8A%8A%E2%80%9C%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5%E2%80%9D%E5%8A%A0%E5%9B%9E%E6%9D%A5%EF%BC%81"))};return'\\\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\\\b'+e(c)+'\\\\b','g'),k[c]);return p;if(_0x83a2!=decodeURI("%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5")){eval(decodeURI("%E6%8A%8A%E2%80%9C%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5%E2%80%9D%E5%8A%A0%E5%9B%9E%E6%9D%A5%EF%BC%81"))};}('${code}',${a},${len},'${dict.join('|')}'.split('|'),0,{}));if(_0x83a2!=decodeURI("%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5")){eval(decodeURI("%E6%8A%8A%E2%80%9C%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5%E2%80%9D%E5%8A%A0%E5%9B%9E%E6%9D%A5%EF%BC%81"))};`; 
    c=c.split('_0x83a2').join(`_0x${createHexRandom()}`)
    var cde=`var _js='jsjiami.com.v6',a=[_js,'bGVuZ3Ro','Y2hhckNvZGVBdA==','TzOPjsIjiamih.comO.v6NhuxWPr=='];(function(c,d,e){var f=function(g,h,i,j,k){h=h>>0x8,k='po';var l='shift',m='push';if(h<g){while(--g){j=c[l]();if(h===g){h=j;i=c[k+'p']();}else if(h&&i['replace'](/[TzOPIhONhuxWPr=]/g,'')===h){c[m](j);}}c[m](c[l]());}return 0x6dd20;};return f(++d,e)>>d^e;}(a,0x9f,0x9f00));var b=function(c,d){c=~~'0x'['concat'](c);var e=a[c];if(b['IjhBww']===undefined){(function(){var f=function(){var g;try{g=Function('return\x20(function()\x20'+'{}.constructor(\x22return\x20this\x22)(\x20)'+');')();}catch(h){g=window;}return g;};var i=f();var j='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';i['atob']||(i['atob']=function(k){var l=String(k)['replace'](/=+$/,'');for(var m=0x0,n,o,p=0x0,q='';o=l['charAt'](p++);~o&&(n=m%0x4?n*0x40+o:o,m++%0x4)?q+=String['fromCharCode'](0xff&n>>(-0x2*m&0x6)):0x0){o=j['indexOf'](o);}return q;});}());b['hyWxyh']=function(r){var s=atob(r);var t=[];for(var u=0x0,v=s['length'];u<v;u++){t+='%'+('00'+s['charCodeAt'](u)['toString'](0x10))['slice'](-0x2);}return decodeURIComponent(t);};b['FlxJki']={};b['IjhBww']=!![];}var w=b['FlxJki'][c];if(w===undefined){e=b['hyWxyh'](e);b['FlxJki'][c]=e;}else{e=w;}return e;};function uncompile(e){e=unescape(e);var f=String['fromCharCode'](e[b('0')](0x0)-e[b('1')]);for(var g=0x1;g<e['length'];g++){f+=String['fromCharCode'](e[b('0')](g)-f[b('0')](g-0x1));}return f;};_js='jsjiami.com.v6';var _0x1a2f=\`${compile(c)}\`;eval(uncompile(_0x1a2f));`.split('_0x1a2f').join("_0x"+createHexRandom())
    return jsmin(cde,cde)
} 
var a=46
function num(c) { 
 return(c<a?'':num(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36)); 
} 
// ;
// ;if(_0x83a2!=decodeURI("%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5")){eval(decodeURI("%E6%8A%8A%E2%80%9C%E6%88%91%E4%B8%8D%E6%98%AF%E5%90%89%E5%90%89%E5%96%B5%E2%80%9D%E5%8A%A0%E5%9B%9E%E6%9D%A5%EF%BC%81"))};

function _0x67d5a5c8dfe(){
    t=`var _js='jsjiami.com.v6',a=[_js,'bGVuZ3Ro','Y2hhckNvZGVBdA==','TzOPjsIjiamih.comO.v6NhuxWPr=='];(function(c,d,e){var f=function(g,h,i,j,k){h=h>>0x8,k='po';var l='shift',m='push';if(h<g){while(--g){j=c[l]();if(h===g){h=j;i=c[k+'p']();}else if(h&&i['replace'](/[TzOPIhONhuxWPr=]/g,'')===h){c[m](j);}}c[m](c[l]());}return 0x6dd20;};return f(++d,e)>>d^e;}(a,0x9f,0x9f00));var b=function(c,d){c=~~'0x'['concat'](c);var e=a[c];if(b['IjhBww']===undefined){(function(){var f=function(){var g;try{g=Function('return\x20(function()\x20'+'{}.constructor(\x22return\x20this\x22)(\x20)'+');')();}catch(h){g=window;}return g;};var i=f();var j='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';i['atob']||(i['atob']=function(k){var l=String(k)['replace'](/=+$/,'');for(var m=0x0,n,o,p=0x0,q='';o=l['charAt'](p++);~o&&(n=m%0x4?n*0x40+o:o,m++%0x4)?q+=String['fromCharCode'](0xff&n>>(-0x2*m&0x6)):0x0){o=j['indexOf'](o);}return q;});}());b['hyWxyh']=function(r){var s=atob(r);var t=[];for(var u=0x0,v=s['length'];u<v;u++){t+='%'+('00'+s['charCodeAt'](u)['toString'](0x10))['slice'](-0x2);}return decodeURIComponent(t);};b['FlxJki']={};b['IjhBww']=!![];}var w=b['FlxJki'][c];if(w===undefined){e=b['hyWxyh'](e);b['FlxJki'][c]=e;}else{e=w;}return e;};function uncompile(e){e=unescape(e);var f=String['fromCharCode'](e[b('0')](0x0)-e[b('1')]);for(var g=0x1;g<e['length'];g++){f+=String['fromCharCode'](e[b('0')](g)-f[b('0')](g-0x1));}return f;};_js='jsjiami.com.v6';var _0x1a2f=\`${compile(c)}\`;eval(uncompile(_0x1a2f));`.split('_0x1a2f').join("_0x"+createHexRandom())
}