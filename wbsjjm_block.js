Blockly.Blocks['event_oninteract']={init:function(){
this.appendValueInput("E").setCheck(null).appendField("当");
this.appendDummyInput().appendField("被交互时执行");
this.appendStatementInput("DO").setCheck(null);
this.setInputsInline(true);
this.setPreviousStatement(true,null);
this.setNextStatement(true,null);
this.setColour(90);
this.setTooltip("");
this.setHelpUrl("");
}};
Blockly.JavaScript['event_oninteract']=(a)=>{
var entity=Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE),Do=Blockly.JavaScript.valueToCode(a, "DO", Blockly.JavaScript.ORDER_NONE)
return `${entity}.onInteract(({entity})=>{${Do}})`
}


Blockly.Blocks['sql_evc'] = {init: function() {
this.appendValueInput("P") 
.setCheck(null)
.appendField("执行SQL：");
this.setPreviousStatement(true, null);
this.setNextStatement(true, null);
this.setColour(21); 
this.setTooltip(""); 
this.setHelpUrl("");
}};
Blockly.JavaScript['sql_evc']=(a=>"\ndb.sql(" + Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_NONE) + ")\n")

Blockly.Blocks['bgmusic'] = {init: function() {this.appendValueInput("NAME").setCheck("String").appendField("设置 世界的 背景音乐 为");this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(230); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.JavaScript['bgmusic']=(a=>"\nworld.ambientSound.sample=`audio/`+" + Blockly.JavaScript.valueToCode(a, "NAME", Blockly.JavaScript.ORDER_NONE) + "\n")

Blockly.Blocks['pro'] = {init: function() {this.appendDummyInput().appendField("作品名称");this.setOutput(true, null);this.setColour(230); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.JavaScript['pro']=(a=>"\nworld.projectName\n")