Blockly.Blocks['rgbcolor'] = {init: function() {this.appendValueInput("R") .setCheck("Number") .appendField("颜色：红色");this.appendValueInput("G").setCheck("Number").appendField("绿色");this.appendValueInput("B").setCheck("Number") .appendField("蓝色");this.setInputsInline(true);this.setOutput(true, "rgbcolor");this.setColour(188); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['world_say'] = {init: function() {this.appendValueInput("M") .setCheck("String").appendField("广播消息");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(270); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['event_onplayerjoin'] = {init: function() {this.appendStatementInput("DO") .setCheck(null).appendField("当玩家进入时执行");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(280); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['event_var_entity'] = {init: function() {this.appendDummyInput().appendField("实体");this.setInputsInline(true);this.setOutput(true, "entity");this.setColour(280); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['rgbacolor'] = {init: function() {this.appendValueInput("R").setCheck("Number").appendField("带透明度的颜色 : 红色");this.appendValueInput("G").setCheck("Number").appendField("绿色");this.appendValueInput("B").setCheck("Number").appendField("蓝色");this.appendValueInput("A").setCheck("Number").appendField("透明度");this.setInputsInline(true);this.setOutput(true, "rgbacolor");this.setColour(188); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_getplayer'] = {init: function() {this.appendValueInput("E").setCheck("entity").appendField("从");this.appendDummyInput().appendField("获得玩家对象");this.setInputsInline(true);this.setOutput(true, "player");this.setColour(99); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_getname'] = {init: function() {this.appendValueInput("P").setCheck(null).appendField("获取");this.appendDummyInput().appendField("的昵称");this.setInputsInline(true);this.setOutput(true, "entity");this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['adv_runcode'] = {init: function() {this.appendValueInput("C").setCheck(null).appendField("运行");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_setfly'] = {init: function() {this.appendValueInput("P").setCheck(null).appendField("玩家");this.appendDummyInput().appendField(new Blockly.FieldDropdown([["可以","T"], ["不可以","F"]]), "BOOL").appendField("飞行");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['event_ontick'] = {init: function() {this.appendStatementInput("DO").setCheck(null).setAlign(Blockly.ALIGN_RIGHT).appendField("当触发TICK时执行");this.setInputsInline(false);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['event_onentitycontact'] = {init: function() {this.appendValueInput("E").setCheck(null).appendField("当实体");this.appendStatementInput("DO").setCheck(null).setAlign(Blockly.ALIGN_RIGHT).appendField("与其他实体碰撞时");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_getbytag'] = {  init: function() {    this.appendValueInput("T")        .setCheck(null)        .appendField("获取标签为");    this.appendDummyInput()        .appendField("的实体");    this.setOutput(true, null);    this.setColour(290); this.setTooltip(""); this.setHelpUrl("");  }};
Blockly.Blocks['event_var_other'] = {init: function() {this.appendDummyInput().appendField("另一个实体");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['position'] = {init: function() {this.appendValueInput("X").setCheck(null).appendField("位置 X");this.appendValueInput("Y").setCheck(null).appendField("Y");this.appendValueInput("Z").setCheck(null).appendField("Z");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_setposition'] = {init: function() {this.appendValueInput("E").setCheck(null).appendField("移动实体");this.appendValueInput("S").setCheck(null).appendField("到");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_setvisible'] = {init: function() {this.appendDummyInput().appendField(new Blockly.FieldDropdown([["显示","false"], ["隐藏","true"]]), "BOOL");this.appendValueInput("NAME").setCheck(null).appendField("玩家");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_hurt'] = {init: function() {this.appendValueInput("E").setCheck(null).appendField("伤害");this.appendValueInput("V").setCheck("Number");this.appendDummyInput().appendField("hp");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_setarg'] = {init: function() {this.appendValueInput("E").setCheck(null).appendField("设置");this.appendDummyInput().appendField("的").appendField(new Blockly.FieldDropdown([["血量","hp"], ["最大血量","maxhp"]]), "O").appendField("为");this.appendValueInput("V").setCheck("Number");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_getarg'] = {init: function() {this.appendValueInput("E").setCheck(null).appendField("获取");this.appendDummyInput().appendField("的").appendField(new Blockly.FieldDropdown([["血量","hp"], ["最大血量","maxHp"]]), "O");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['event_onentityclick'] = {init: function() {this.appendValueInput("E").setCheck(null).appendField("当实体");this.appendDummyInput().appendField("被点击时执行");this.appendStatementInput("DO").setCheck(null);this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['event_var_clicker'] = {init: function() {this.appendDummyInput().appendField("点击的玩家");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_say'] = {init: function() {this.appendValueInput("E").setCheck(null).appendField("实体");this.appendValueInput("M").setCheck(null).appendField("说");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_getposition'] = {init: function() {this.appendValueInput("E").setCheck(null).appendField("获取");this.appendDummyInput().appendField("的").appendField(new Blockly.FieldDropdown([["x","x"], ["y","y"], ["z","z"]]), "V").appendField("坐标");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['world_setarg'] = {init: function() {this.appendDummyInput().appendField("设置世界的").appendField(new Blockly.FieldDropdown([["重力大小","gravity"], ["空气阻力","airFriction"]]), "O");this.appendValueInput("V").setCheck(null).appendField("为");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_foreach'] = {init: function() {this.appendDummyInput().appendField("让所有").appendField(new Blockly.FieldDropdown([["玩家","player"], ["实体","*"]]), "T").appendField("执行");this.appendStatementInput("DO").setCheck(null);this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['adv_blockclamp'] = {init: function() {this.appendDummyInput().appendField("积木夹子");this.appendStatementInput("DO").setCheck(null);this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_setcolor'] = {init: function() {this.appendValueInput("P").setCheck(null).appendField("设置玩家");this.appendValueInput("C").setCheck(null).appendField("的颜色为");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['event_onchat'] = {init: function() {this.appendDummyInput().appendField("当实体说话时执行");this.appendStatementInput("DO").setCheck(null);this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['event_var_message'] = {init: function() {this.appendDummyInput().appendField("说话内容");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_isplayer'] = {init: function() {this.appendValueInput("E").setCheck(null).appendField("实体");this.appendDummyInput().appendField("是玩家");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_direct'] = {init: function() {this.appendValueInput("M").setCheck(null).appendField("私信");this.appendValueInput("P").setCheck(null).appendField("给");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_setarg'] = {init: function() {this.appendValueInput("P").setCheck(null).appendField("设置");this.appendDummyInput().appendField("的").appendField(new Blockly.FieldDropdown([["缩放","scale"]]), "O");this.appendValueInput("V").setCheck(null).appendField("为");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_setvelocity'] = {init: function() {this.appendValueInput("E").setCheck(null).appendField("设置");this.appendValueInput("X").setCheck(null).appendField("的速度为").appendField("x");this.appendValueInput("Y").setCheck(null).appendField("y");this.appendValueInput("Z").setCheck(null).appendField("z");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['event_ondie'] = {init: function() {this.appendDummyInput().appendField("当实体死亡时执行");this.appendStatementInput("DO").setCheck(null);this.setInputsInline(false);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_setspawnpoint'] = {init: function() {this.appendValueInput("P").setCheck(null).appendField("设置");this.appendValueInput("O").setCheck(null).appendField("的出生点为");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['debug_log'] = {init: function() {this.appendValueInput("V").setCheck(null).appendField("调试信息");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['debug_warn'] = {init: function() {this.appendValueInput("V").setCheck(null).appendField("警告");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['debug_clear'] = {init: function() {this.appendDummyInput().appendField("清空调试信息");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['debug_error'] = {init: function() {this.appendValueInput("V").setCheck(null).appendField("输出错误");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['event_init'] = {init: function() {this.appendDummyInput().appendField("当开始运行时");this.setInputsInline(true);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_setattr'] = {init: function() {this.appendValueInput("P").setCheck(null).appendField("设置");this.appendValueInput("ATR").setCheck(null).appendField("的私有变量");this.appendValueInput("V").setCheck(null).appendField("为");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_movestate'] = {init: function() {this.appendDummyInput().appendField(new Blockly.FieldDropdown([["飞行中","fly"], ["在地上","ground"], ["游泳中","swim"], ["下落中","fall"], ["跳跃中","jump"], ["二段跳中","jump2"]]), "S");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_getmovestate'] = {init: function() {this.appendValueInput("P").setCheck(null).appendField("获取");this.appendDummyInput().appendField("的运动状态");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_getwalkstate'] = {init: function() {this.appendValueInput("P").setCheck(null).appendField("获取");this.appendDummyInput().appendField("的行走状态");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_walkstate'] = {init: function() {this.appendDummyInput().appendField(new Blockly.FieldDropdown([["非行走中","none"], ["下蹲行走","crouch"], ["正常行走","walk"], ["奔跑","run"]]), "S");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['player_forcerespawn'] = {  init: function() {    this.appendValueInput("P")        .setCheck(null)        .appendField("强制重生");    this.setPreviousStatement(true, null);    this.setNextStatement(true, null);    this.setColour(90); this.setTooltip(""); this.setHelpUrl("");  }};
Blockly.Blocks['player_getattr'] = {init: function() {this.appendValueInput("P").setCheck(null).appendField("获取");this.appendValueInput("T").setCheck(null).appendField("的私有变量");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_enabledamage'] = {init: function() {this.appendValueInput("E").setCheck(null);this.appendDummyInput().appendField(new Blockly.FieldDropdown([["可以","true"], ["不可以","false"]]), "O").appendField("受到伤害");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['entity_showhealthbar'] = {init: function() {this.appendValueInput("E").setCheck(null);this.appendDummyInput().appendField(new Blockly.FieldDropdown([["显示","true"], ["不显示","false"]]), "O").appendField("血量条");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['block_getid1'] = {init: function() {this.appendDummyInput().appendField("自然方块").appendField(new Blockly.FieldDropdown([["泥土", "dirt"], ["草地", "grass"], ["石头", "stone"], ["绿色树叶", "green_leaf"], ["普通树干", "acacia"], ["沙子", "sand"], ["雪", "snow"], ["青色树叶", "leaf_01"], ["红色树叶", "leaf_02"], ["橡木树干", "wood"], ["深色草地", "dark_grass"], ["深色石头", "dark_stone"], ["雪地", "snowland"], ["结冰雪地", "polar_region"], ["结冰土块", "polar_ice"], ["蓝色山壁", "blue_surface_01"], ["蓝色石壁", "blue_surface_02"], ["紫色光滑草地", "purple_surface_01"], ["紫色凹凸草地", "purple_surface_02"], ["深色凹凸石头", "dark_surface"], ["裂石", "rock"], ["水", "water"], ["冰块", "ice"]]), "O");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['block_getid2'] = {init: function() {this.appendDummyInput().appendField("建筑方块").appendField(new Blockly.FieldDropdown([["花窗", "stained_glass"], ["加工木板", "plank_01"], ["普通木板", "plank_02"], ["杉木木板", "plank_03"], ["加工木条", "plank_04"], ["冰块地板", "ice_brick"], ["亮色石灰石", "light_grey_stone_brick"], ["坚硬石灰石", "grey_stone_brick"], ["金边条纹块", "gold_trim_brick"], ["红砖", "re_brick"], ["石英砖块", "quartz_brick"], ["萤石灯", "lantern_01"], ["白炽灯", "lantern_02"], ["双格窗", "window"], ["框架窗", "cross_window"], ["花边窗", "geometric_window_01"], ["螺旋边窗", "geometric_window_02"], ["玻璃", "glass"], ["彩色玻璃", "color_glass"], ["木箱", "wooden_box"], ["平滑木板", "board_01"], ["深色平滑木板", "board_02"], ["黄色地毯", "carpet_01"], ["蓝色地毯", "carpet_02"], ["粉色地毯", "carpet_03"], ["绿色地毯", "carpet_04"], ["红色地毯", "carpet_05"], ["灰色地毯", "carpet_06"], ["暗红色地毯", "carpet_07"], ["宫殿屋顶1", "palace_eaves_01"], ["宫殿屋顶2", "palace_eaves_02"], ["宫殿屋顶3", "palace_eaves_03"], ["宫殿屋顶4", "palace_eaves_04"], ["宫殿屋顶5", "palace_eaves_05"], ["宫殿屋顶6", "palace_eaves_06"], ["宫殿屋顶7", "palace_eaves_07"], ["宫殿屋顶8", "palace_eaves_08"], ["红色瓷砖", "roof_red"], ["紫色瓷砖", "roof_purple"], ["绿色瓷砖", "roof_green"], ["蓝色瓷砖", "roof_blue_04"], ["黄色瓷砖", "roof_yellow"], ["马赛克", "carpet_08"], ["蓝色方格", "carpet_09"], ["绿色方格", "carpet_10"], ["黄色方格", "carpet_11"], ["紫色方格", "carpet_12"], ["红色方格", "carpet_13"], ["蓝色方格", "carpet_09"], ["纯金属", "stainless_steel"], ["冰墙", "ice_wall"], ["宫殿地板", "palace_roof"], ["红色地板", "red_brick_floor"], ["神秘地板", "palace_floor"], ["透明浮雕", "palace_carving"], ["石雕1", "stone_paillar_03"], ["石雕2", "stone_paillar_04"], ["石雕3", "stone_paillar_05"], ["石雕4", "stone_paillar_06"], ["石墙", "stone_wall"], ["蓝色玻璃", "blue_glass"], ["绿色玻璃", "green_glass"], ["黑色玻璃", "black_glass"], ["红色玻璃", "red_glass"], ["灯笼", "palace_lamp"], ["蓝色玻璃", "blue_glass"], ["松散木材", "board_03"], ["橡木木箱", "board_04"], ["深色橡木木箱", "board_05"], ["杉木木箱", "board_06"], ["雏菊花圃", "greenbelt_L"], ["玫瑰花圃", "greenbelt_L1"], ["石灰墙", "stone_brick_01"], ["松散石灰墙", "stone_brick_02"], ["深色石灰墙", "dark_brick_00"], ["松散深色石灰墙", "dark_brick_01"], ["螺旋深色石灰墙", "dark_brick_02"], ["亮色石墙", "stone_wall_01"], ["筋斗云", "palace_cloud"], ["浮雕地板1", "crane_roof_01"], ["浮雕地板2", "crane_roof_02"], ["暗色灯笼", "crane_lantern"], ["灰色瓷砖", "roof_grey"], ["红花窗", "palace_window"], ["暗色灯笼", "crane_lantern"], ["木条石墙", "woodstone_12"]]), "O");this.setInputsInline(true);this.setOutput(true);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['block_set'] = {init: function() {this.appendValueInput("P").setCheck(null).appendField("设置");this.appendValueInput("V").setCheck(null).appendField("处的方块为");this.setInputsInline(true);this.setPreviousStatement(true, null);this.setNextStatement(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['block_getid3'] = {init: function() {this.appendDummyInput().appendField("纯色方块").appendField(new Blockly.FieldDropdown([["灰蓝色", "cadet_blue"], ["天蓝色", "sky_blue"], ["浅蓝色", "OPTIONNAME"], ["深灰色", "dark_gray"], ["浅灰色", "light_gray"], ["橄榄绿", "olive_green"], ["叶绿色", "yellow_green"], ["浅绿色", "pale_green"], ["红色", "red"], ["深红色", "dark_red"], ["砖红色", "brick_red"], ["灰色", "medium_gray"], ["地板蓝", "dark_slate_blue"], ["粉色", "pink"], ["淡粉色", "sakura_pink"], ["橘色", "orange"], ["柠檬色", "lemon"], ["黑色", "black"], ["白色", "white"], ["红色条纹", "stripe_01"], ["蓝色条纹", "stripe_02"], ["黄色条纹", "stripe_03"], ["绿色条纹", "stripe_04"], ["紫色条纹", "stripe_05"], ["蓝色", "blue"], ["青色", "turquoise"], ["深紫色", "dark_orchid"], ["淡紫色", "medium_orchid"], ["蓝紫色", "medium_purple"], ["洋红色", "medium_violet_red"], ["褐色", "maroon"], ["咖啡色", "coffee_gray"], ["土色", "peru"], ["土黄色", "dark_salmon"], ["黄白色", "navajo_white"], ["橘红色", "orange_red"], ["黄色", "medium_yellow"], ["深绿色", "medium_green"], ["浅褐色", "sienna"], ["亮绿色", "mint_green"], ["青绿色", "medium_spring_green"]]), "O");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['block_getid4'] = {init: function() {this.appendDummyInput().appendField("字母方块").appendField(new Blockly.FieldDropdown([["A", "A"], ["B", "B"], ["C", "C"], ["D", "D"], ["E", "E"], ["F", "F"], ["G", "G"], ["H", "H"], ["I", "I"], ["J", "J"], ["L", "L"], ["M", "M"], ["N", "N"], ["O", "O"], ["P", "P"], ["Q", "Q"], ["R", "R"], ["S", "S"], ["T", "T"], ["U", "U"], ["V", "V"], ["W", "W"], ["X", "X"], ["Y", "Y"], ["Z", "Z"]]), "O");this.setInputsInline(true);this.setOutput(true);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['block_getid5'] = {init: function() {this.appendDummyInput().appendField("数字方块").appendField(new Blockly.FieldDropdown([["0", "zero"], ["1", "one"], ["2", "two"], ["3", "three"], ["5", "five"], ["6", "six"], ["7", "seven"], ["8", "eight"], ["9", "nine"]]), "O");this.setInputsInline(true);this.setOutput(true);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['block_getid6'] = {init: function() {
    this.appendDummyInput().appendField("灯光方块").appendField(new Blockly.FieldDropdown([["红色灯", "red_light"], ["橙色灯", "orange_light"], ["黄色灯", "yellow_light"], ["绿色灯", "green_light"], ["青色灯", "indigo_light"], ["蓝色灯", "blue_light"], ["紫色灯", "purple"], ["粉色灯", "pink_light"], ["淡绿色灯", "mint_green_light"], ["白色灯", "white_light"], ["暖色灯", "warm_yellow_light"]]), "O");
    this.setInputsInline(true);
    this.setOutput(true);
    this.setColour(90); 
    this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['block_get'] = {init: function() {this.appendValueInput("P").setCheck(null).appendField("获取");this.appendDummyInput().appendField("处的方块");this.setInputsInline(true);this.setOutput(true, null);this.setColour(90); this.setTooltip(""); this.setHelpUrl("");}};
Blockly.Blocks['includs'] = {  init: function() {    this.appendValueInput("NAME")        .setCheck(null)        .appendField("列表");    this.appendValueInput("m")        .setCheck(null)        .appendField("是否包含");    this.setInputsInline(true);    this.setOutput(true, null); this.setTooltip(""); this.setColour(260);this.setHelpUrl("");  }};
window.usjs = !1,
Blockly.JavaScript.rgbcolor = (a=>(R = Blockly.JavaScript.valueToCode(a, "R", Blockly.JavaScript.ORDER_NONE) || 1,
G = Blockly.JavaScript.valueToCode(a, "G", Blockly.JavaScript.ORDER_NONE) || 1,
B = Blockly.JavaScript.valueToCode(a, "B", Blockly.JavaScript.ORDER_NONE) || 1,
["new Box3RGBColor(" + R + "," + G + "," + B + ")", Blockly.JavaScript.ORDER_ATOMIC])),
Blockly.JavaScript.world_say = (a=>"world.say(" + Blockly.JavaScript.valueToCode(a, "M", Blockly.JavaScript.ORDER_NONE) + ");\n"),
Blockly.JavaScript.event_onplayerjoin = (a=>"world.onPlayerJoin(async ({entity})=>{\n" + Blockly.JavaScript.statementToCode(a, "DO") + "\n});\n"),
Blockly.JavaScript.event_onentityclick = (a=>Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE) + ".onClick(async ({entity,clicker,button})=>{\n" + Blockly.JavaScript.statementToCode(a, "DO") + "\n});\n"),
Blockly.JavaScript.event_ontick = (a=>"world.onTick((evt)=>{\n" + Blockly.JavaScript.statementToCode(a, "DO") + "\n});\n"),
Blockly.JavaScript.event_var_entity = (a=>["entity", Blockly.JavaScript.ORDER_ATOMIC]),
Blockly.JavaScript.event_var_other = (a=>["other", Blockly.JavaScript.ORDER_ATOMIC]),
Blockly.JavaScript.event_var_clicker = (a=>["clicker", Blockly.JavaScript.ORDER_ATOMIC]),
Blockly.JavaScript.event_var_message = (a=>["message", Blockly.JavaScript.ORDER_ATOMIC]),
Blockly.JavaScript.adv_runcode = (a=>"\neval(" + Blockly.JavaScript.valueToCode(a, "C", Blockly.JavaScript.ORDER_NONE) + ")\n"),
Blockly.JavaScript.player_getname = (a=>[Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_NONE) + ".name", Blockly.JavaScript.ORDER_ATOMIC]),
Blockly.JavaScript.entity_getplayer = (a=>[Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE) + ".player", Blockly.JavaScript.ORDER_ATOMIC]),
Blockly.JavaScript.player_setfly = (a=>Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_NONE) + ".canFly=" + {
T: "true",
F: "false"
}[a.getFieldValue("BOOL")]),
Blockly.JavaScript.event_onentitycontact = (a=>Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE) + ".onEntityContact(async ({entitiy,other})=>{\n" + Blockly.JavaScript.statementToCode(a, "DO") + "\n});\n"),
Blockly.JavaScript.entity_getbytag = (a=>['world.querySelector("."+' + Blockly.JavaScript.valueToCode(a, "T", Blockly.JavaScript.ORDER_NONE) + ")", Blockly.JavaScript.ORDER_ATOMIC]),
Blockly.JavaScript.entity_setposition = (a=>Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE) + ".position=" + Blockly.JavaScript.valueToCode(a, "S", Blockly.JavaScript.ORDER_NONE) + ";\n"),
Blockly.JavaScript.position = (a=>["new Box3Vector3(" + Blockly.JavaScript.valueToCode(a, "X", Blockly.JavaScript.ORDER_NONE) + "," + Blockly.JavaScript.valueToCode(a, "Y", Blockly.JavaScript.ORDER_NONE) + "," + Blockly.JavaScript.valueToCode(a, "Z", Blockly.JavaScript.ORDER_NONE) + ")", Blockly.JavaScript.ORDER_ATOMIC]),
Blockly.JavaScript.player_setvisible = (a=>Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_NONE) + ".invisible=" + a.getFieldValue("BOOL") + ";\n"),
Blockly.JavaScript.entity_hurt = (a=>Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE) + ".hurt(" + Blockly.JavaScript.valueToCode(a, "V", Blockly.JavaScript.ORDER_NONE) + ");\n"),
Blockly.JavaScript.entity_say = (a=>Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE) + ".say(" + Blockly.JavaScript.valueToCode(a, "M", Blockly.JavaScript.ORDER_NONE) + ");\n"),
Blockly.JavaScript.entity_setarg = (a=>Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE) + "." + a.getFieldValue("O") + "=" + Blockly.JavaScript.valueToCode(a, "V", Blockly.JavaScript.ORDER_NONE) + ";\n"),
Blockly.JavaScript.player_setarg = (a=>Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_NONE) + "." + a.getFieldValue("O") + "=" + Blockly.JavaScript.valueToCode(a, "V", Blockly.JavaScript.ORDER_NONE) + ";\n"),
Blockly.JavaScript.entity_getarg = (a=>[Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE) + "." + a.getFieldValue("O"), Blockly.JavaScript.ORDER_ATOMIC]),
Blockly.JavaScript.world_setarg = (a=>"world." + a.getFieldValue("O") + "=" + Blockly.JavaScript.valueToCode(a, "V", Blockly.JavaScript.ORDER_NONE) + ";\n"),
Blockly.JavaScript.entity_getposition = (a=>[Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE) + ".position." + a.getFieldValue("V"), Blockly.JavaScript.ORDER_ATOMIC]),
Blockly.JavaScript.entity_foreach = (a=>'world.querySelectorAll("' + a.getFieldValue("T") + '").forEach((entity)=>{\n' + Blockly.JavaScript.statementToCode(a, "DO") + "\n});\n"),
Blockly.JavaScript.adv_blockclamp = (a=>Blockly.JavaScript.statementToCode(a, "DO")),
Blockly.JavaScript.logic_wait = (a=>"await sleep(" + 1e3 * Blockly.JavaScript.valueToCode(a, "V", Blockly.JavaScript.ORDER_NONE) + ");\n"),
Blockly.JavaScript.player_setcolor = (a=>Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_NONE) + ".color=" + Blockly.JavaScript.valueToCode(a, "C", Blockly.JavaScript.ORDER_NONE) + ";\n"),
Blockly.JavaScript.event_onchat = (a=>"world.onChat(async ({entity,message})=>{if(entity){\n" + Blockly.JavaScript.statementToCode(a, "DO") + "\n}});\n"),
Blockly.JavaScript.event_ondie = (a=>"world.onDie(async ({entity,attacker})=>{\n" + Blockly.JavaScript.statementToCode(a, "DO") + "\n});\n"),
Blockly.JavaScript.entity_isplayer = (a=>[Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE) + ".isPlayer", Blockly.JavaScript.ORDER_ATOMIC]),
Blockly.JavaScript.player_direct = (a=>Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_NONE) + ".directMessage(" + Blockly.JavaScript.valueToCode(a, "M", Blockly.JavaScript.ORDER_NONE) + ");\n"),
Blockly.JavaScript.entity_setvelocity = (a=>Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_NONE) + ".velocity=new Box3Vector3(" + Blockly.JavaScript.valueToCode(a, "X", Blockly.JavaScript.ORDER_NONE) + "," + Blockly.JavaScript.valueToCode(a, "Y", Blockly.JavaScript.ORDER_NONE) + "," + Blockly.JavaScript.valueToCode(a, "Z", Blockly.JavaScript.ORDER_NONE) + ",);\n"),
Blockly.JavaScript.block_getid1 = Blockly.JavaScript.block_getid2 = Blockly.JavaScript.block_getid3 = Blockly.JavaScript.block_getid4 = Blockly.JavaScript.block_getid5 = Blockly.JavaScript.block_getid6 = Blockly.JavaScript.block_getid7 = Blockly.JavaScript.block_getid8 = (a=>['voxels.id("' + a.getFieldValue("O") + '")', Blockly.JavaScript.ORDER_NONE]),
Blockly.JavaScript.block_set = (a=>(p = Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_ATOMIC),
x = p + ".x",
y = p + ".y",
z = p + ".z",
`voxels.setVoxel(${x},${y},${y},${Blockly.JavaScript.valueToCode(a, "V", Blockly.JavaScript.ORDER_ATOMIC)});\n`)),
Blockly.JavaScript.block_get = (a=>(p = Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_ATOMIC),
x = p + ".x",
y = p + ".y",
z = p + ".z",
[`voxels.getVoxel(${x},${y},${y})`, Blockly.JavaScript.ORDER_NONE])),
Blockly.JavaScript.player_setspawnpoint = function(a) {
var l = Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_ATOMIC)
, c = Blockly.JavaScript.valueToCode(a, "O", Blockly.JavaScript.ORDER_ATOMIC)
, o = `${l}.spawnPoint=${c};\n`;
return o
}
,
Blockly.JavaScript.debug_log = function(a) {
var l = Blockly.JavaScript.valueToCode(a, "V", Blockly.JavaScript.ORDER_ATOMIC)
, c = `console.log(${l});\n`;
return c
}
,
Blockly.JavaScript.debug_clear = function(a) {
var l = "console.clear();\n";
return l
}
,
Blockly.JavaScript.debug_warn = function(a) {
var l = Blockly.JavaScript.valueToCode(a, "V", Blockly.JavaScript.ORDER_ATOMIC)
, c = `console.warn(${l});\n`;
return c
}
,
Blockly.JavaScript.debug_error = function(a) {
var l = Blockly.JavaScript.valueToCode(a, "V", Blockly.JavaScript.ORDER_ATOMIC)
, c = `console.error(${l});\n`;
return c
}
,
Blockly.JavaScript.logic_intext = function(a) {
var l = Blockly.JavaScript.valueToCode(a, "A", Blockly.JavaScript.ORDER_ATOMIC)
, c = Blockly.JavaScript.valueToCode(a, "B", Blockly.JavaScript.ORDER_ATOMIC)
, o = `"${l}".indexOf("${c}")!=-1`;
return [o, Blockly.JavaScript.ORDER_NONE]
}
,
Blockly.JavaScript.player_setattr = function(a) {
var l = Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_ATOMIC)
, c = a.getFieldValue("ATR")
, o = Blockly.JavaScript.valueToCode(a, "V", Blockly.JavaScript.ORDER_ATOMIC)
, t = `${l}.${c}=${o};\n`;
return t
}
,
Blockly.JavaScript.debug_clear = function() {
var a = "console.clear();\n";
return a
}
,
Blockly.JavaScript.event_init = function(a) {
return ""
}
,
Blockly.JavaScript.player_movestate = function(a) {
var l = a.getFieldValue("S")
, c = `${l}`;
return [c, Blockly.JavaScript.ORDER_NONE]
}
,
Blockly.JavaScript.player_getmovestate = function(a) {
var l = Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_ATOMIC)
, c = `${l}.moveState`;
return [c, Blockly.JavaScript.ORDER_NONE]
}
,
Blockly.JavaScript.player_walkstate = function(a) {
var l = a.getFieldValue("S")
, c = `${l}`;
return [c, Blockly.JavaScript.ORDER_NONE]
}
,
Blockly.JavaScript.player_getwalkstate = function(a) {
var l = Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_ATOMIC)
, c = `${l}.walkState`;
return [c, Blockly.JavaScript.ORDER_NONE]
}
,
Blockly.JavaScript.player_forcerespawn = function(a) {
var l = Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_ATOMIC)
, c = `${l}.forceRespawn();\n`;
return c
}
,
Blockly.JavaScript.player_getattr = function(a) {
var l = Blockly.JavaScript.valueToCode(a, "P", Blockly.JavaScript.ORDER_ATOMIC)
, c = a.getFieldValue("T")
, o = `${l}.${c}`;
return [o, Blockly.JavaScript.ORDER_NONE]
}
,
Blockly.JavaScript.entity_enabledamage = function(a) {
var l = a.getFieldValue("O")
, c = Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_ATOMIC)
, o = `${c}.enableDamage=${l};\n`;
return o
}
,
Blockly.JavaScript.entity_showhealthbar = function(a) {
var l = a.getFieldValue("O")
, c = Blockly.JavaScript.valueToCode(a, "E", Blockly.JavaScript.ORDER_ATOMIC)
, o = `${c}.showHealthBar=${l};\n`;
return o
}
;
Blockly.defineBlocksWithJsonArray([
{
type: "logic_wait",
message0: "等待 %1 秒",
args0: [{
type: "input_value",
name: "V",
check: "Number"
}],
inputsInline: !0,
previousStatement: null,
nextStatement: null,
style: "logic_blocks",
tooltip: "",
helpUrl: ""
},{
type: "logic_intext",
message0: "在 %1 中包含 %2",
args0: [{
type: "input_value",
name: "A",
check: "String"
}, {
type: "input_value",
name: "B",
check: "String"
}],
inputsInline: !0,
output: "Boolean",
style: "logic_blocks",
tooltip: "",
helpUrl: ""
},{
type: "wbsjjm",
message0: "版权归 我不是吉吉喵 所有",
color: 77,
},{
type: "wbsjjm2",
message0: "copyright wbsjjm wbsjjm@outlook.com",
color: 77,
},{
type: "wbsjjm3",
message0: "请勿在任何积木上添加注释,否则将导致生成的代码功能不齐全或报错!!!",
color: 77,
},
])
Blockly.JavaScript['wbsjjm']=function(block){
return `console.log("made by wbsjjm-blockly")`
}
Blockly.JavaScript['wbsjjm1']=function(block){
return `console.log("made by wbsjjm-blockly")`
}
Blockly.JavaScript['wbsjjm3']=function(block){
return `console.log("made by wbsjjm-blockly")`
}
//interact
Blockly.Blocks['entity_enableinteract']={
init:function(){
this.appendValueInput("entity")
.setCheck(null)
.appendField("实体");
this.appendValueInput("bool")
.setCheck("Boolean")
.setAlign(Blockly.ALIGN_RIGHT);
this.appendDummyInput()
.appendField("交互");
this.setInputsInline(true);
this.setPreviousStatement(true,null);
this.setNextStatement(true,null);
this.setColour(290);
this.setTooltip("");
this.setHelpUrl("");
}
};Blockly.JavaScript['entity_enableinteract']=function(block){
var argument0=Blockly.JavaScript.valueToCode(block,'entity',
Blockly.JavaScript.ORDER_FUNCTION_CALL)||'\'\'';
var argument1=Blockly.JavaScript.valueToCode(block,'bool',
Blockly.JavaScript.ORDER_FUNCTION_CALL)||'\'\'';
var code=`${argument0}.enableInteract=${argument1}`;
return code;
};//D:\x\xampp-control.exe









Blockly.JavaScript['includs'] = (a=>[Blockly.JavaScript.valueToCode(a, "NAME", Blockly.JavaScript.ORDER_ATOMIC) + '.includs('+Blockly.JavaScript.valueToCode(a, "m", Blockly.JavaScript.ORDER_ATOMIC)+")", Blockly.JavaScript.ORDER_NONE]);
